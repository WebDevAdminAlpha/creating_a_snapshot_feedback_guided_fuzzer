extern crate nix;
extern crate rand;

use std::env::args;
use std::os::unix::process::CommandExt;
use std::process::exit;
use std::process::Command;
use std::time;

use nix::sys::ptrace;
use nix::sys::signal::Signal;
use nix::sys::wait::{waitpid, WaitStatus};
use nix::unistd::Pid;

mod mutate;

pub fn spawn(cmd: &String, args: &[String]) -> Pid {
    let child = unsafe {
        Command::new(cmd)
            .args(args)
            .pre_exec(|| {
                ptrace::traceme().expect("Could not trace process");
                Ok(())
            })
            .spawn()
            .expect(&format!("Could not spawn {:?} with args {:?}", cmd, args))
    };

    let res = Pid::from_raw(child.id() as i32);

    match waitpid(res, None) {
        Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => {
            ptrace::cont(res, None).expect("Should have continued");
        }
        _ => println!("COULD NOT START"),
    }

    res
}

fn main() {
    let prog_args: Vec<String> = args().collect();
    if prog_args.len() != 3 {
        println!("USAGE: {} CMD CMD_INPUT", prog_args[0]);
        exit(1);
    }

    let orig_input: &[u8] = prog_args[2].as_bytes();
    let mut scratch_space: Vec<u8> = vec![0; orig_input.len()];
    let mut rand = rand::thread_rng();

    let start = time::Instant::now();
    let mut iters = 0;
    let print_status = |x| {
        let end = time::Instant::now();
        println!(
            "{} iters {:.2} iters/s",
            x,
            (x as f64 / (end - start).as_secs_f64()),
        );
    };

    loop {
        iters += 1;
        if iters % 0xfff == 0 {
            print_status(iters);
        }

        scratch_space.copy_from_slice(orig_input);
        mutate::mutate(&mut rand, &mut scratch_space);

        let child_pid = unsafe {
            spawn(
                &prog_args[1],
                &[std::str::from_utf8_unchecked(&scratch_space).to_string()],
            )
        };
        let mut crashed = false;
        match waitpid(child_pid, None) {
            Ok(WaitStatus::Exited(_, _status)) => {
                //println!("Process exited with status {}", status);
            }
            Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => {
                println!("Debugee stopped");
                ptrace::cont(child_pid, None).expect("Should have continued");
            }
            Ok(WaitStatus::Continued(_)) => println!("Continuing"),
            Ok(WaitStatus::PtraceEvent(_, signal, v)) => {
                println!("ptrace event: signal: {:?}, v: {:?}", signal, v);
                crashed = true;
            }
            Ok(WaitStatus::Signaled(_, signal, val)) => {
                println!("ptrace signaled: signal: {:?}, val: {:?}", signal, val);
                crashed = true;
            }
            Ok(WaitStatus::StillAlive) => println!("Still alive"),
            Ok(WaitStatus::Stopped(_, signal)) => {
                println!("Stopped: signal: {:?}", signal);
                crashed = true;
            }
            Ok(WaitStatus::PtraceSyscall(_)) => println!("Syscall"),
            Err(v) => println!("Error!: {:?}", v),
        }
        let _ = ptrace::kill(child_pid);

        if crashed {
            println!("Final stats:");
            print_status(iters);
            println!(
                "Crashed with: {:?}",
                String::from_utf8_lossy(&scratch_space)
            );
            break;
        }
    }
}
