#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

#define PRINT(msg) ({printf("    >> %s\n", msg); fflush(stdout);})

void do_something(char *data) {
    if (strlen(data) < 6) {
        printf("Returning! strlen(data): %lu\n", strlen(data));
        fflush(stdout);
        return;
    }

    if (data[0] == 'G') {
        if (data[1] == 'I') {
            if (data[2] == 'T') {
                if (data[3] == 'L') {
                    if (data[4] == 'A') {
                        if (data[5] == 'B') {
                            PRINT(">> CRASH!\n");
                            char *blah = 0;
                            blah[0] = 'A';
                        }
                    }
                }
            }
        }
    }
}

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("USAGE: %s DATA\n", argv[0]);
        return 1;
    }

    do_something(argv[1]);
}
