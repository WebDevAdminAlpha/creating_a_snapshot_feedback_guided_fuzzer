use std::env::args;
use std::process::exit;
use std::process::Command;

fn main() {
    let prog_args: Vec<String> = args().collect();
    if prog_args.len() == 1 {
        println!("USAGE: {} CMD [CMD_ARG1] [CMD_ARG2]", prog_args[0]);
        exit(1);
    }

    loop {
        let mut proc = Command::new(&prog_args[1])
            .args(&prog_args[2..])
            .spawn()
            .expect("Could not start process");

        let res = proc.wait().expect("Could not wait for process to exit");
        if !res.success() {
            println!("Crash!");
        }
    }
}
